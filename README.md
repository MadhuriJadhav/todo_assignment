# ToDo

ToDo is the Application used for creating Todo Tasks

## Installation

Use Following Commands to run Application

```bash
npm install
npx react-native run-android
```

## Features

1. User can Add New Tasks in Todo List.
2. User can Mark the Tasks as Complete/Incomplete by either clicking on the Task or on the Checkbox.
3. User can delete the created Tasks.



