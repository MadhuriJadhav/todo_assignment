import { connect } from 'react-redux';
import TodoListItem from '../components/TodoListItem';

const TaskItemContainer = connect()(TodoListItem);
export default TaskItemContainer;