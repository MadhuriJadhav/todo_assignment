import {ADD_TODO, TOGGLE_TODO, REMOVE_TODO} from '../actions/actionTypes'

const todos = (state=[], action) => {
    switch (action.type) {
        //Returns a complete list of Todo by adding a new task
        case ADD_TODO:
            return [
                ...state, {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ]
        //After Tapping on the Task, completed status of the todo is toggled
        case TOGGLE_TODO:
        return state.map(todo => 
            (todo.id === action.id) ? {...todo, completed: !todo.completed} : todo
        )

        //After click on delete the Task, that todo is removed from the list
        case REMOVE_TODO:
        return state.filter(({id}) => id !== action.id);
    
        default:
            return state
    }
    return state
}

export default todos

